<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PriceModel extends CI_Model {

	private $TABLE_NAME = 'price_list';

	private $ID_COLUMN = 'id';
	private $NAME_COLUMN = 'name';
	private $VALUE_COLUMN = 'value';

	public function selectAll()
	{
		$query = $this->db->get($this->TABLE_NAME);
		return $query->result();
	}

	public function selectPrice($name)
	{
		$this->db->where($this->NAME_COLUMN, $name);
		$this->db->limit(1);
		$query = $this->db->get($this->TABLE_NAME);
		return $query->row();
	}

}
/*
CREATE TABLE `price_list` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(64) NOT NULL UNIQUE,
    `value` DECIMAL(5, 2) NOT NULL DEFAULT 0.0,
    `daily_multiplier` BOOLEAN NOT NULL DEFAULT FALSE
);
INSERT INTO `price_list` (`name`, `value`, `daily_multiplier`) VALUES ('party', 70.0, FALSE);
INSERT INTO `price_list` (`name`, `value`, `daily_multiplier`) VALUES ('accomodation', 40.0, TRUE);
INSERT INTO `price_list` (`name`, `value`, `daily_multiplier`) VALUES ('meal', 35.0, TRUE);
INSERT INTO `price_list` (`name`, `value`, `daily_multiplier`) VALUES ('sleigh', 15.0, FALSE);
INSERT INTO `price_list` (`name`, `value`, `daily_multiplier`) VALUES ('organization', 7.0, FALSE);
*/