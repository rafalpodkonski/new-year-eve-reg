<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistrationModel extends CI_Model {

	private $TABLE_NAME = 'registration';

	public function selectAll($page = 0, $limit = 5)
	{
		$this->db->limit($limit, $page * $limit);
		$this->db->order_by('id');
		$query = $this->db->get($this->TABLE_NAME);
		$result = $query->result();
		return array(
			'results' => $result,
			'page' => $page,
			'limit' => $limit,
			'items' => $this->db->count_all($this->TABLE_NAME)
		);
	}

	public function count($where = array())
	{
		if (is_array($where) && count($where) > 0) {
			$this->db->where($where);
		}
		return $this->db->count_all_results($this->TABLE_NAME);
	}

	public function registerUser($formData)
	{
		$data = array(
			'hash' => $this->_countHash($formData),
			'name' => html_escape($formData['name']),
			'surname' => html_escape($formData['surname']),
			'email' => html_escape($formData['email']),
			'arrival' => html_escape($formData['arrival']),
			'nights' => html_escape($formData['nights_no']),
			'meals' => $formData['with_meal'] == 'on',
			'lects' => html_escape($formData['travel_lect_subj']),
			'organization_help' => html_escape($formData['org_help']),
			'sleigh' => $formData['sleigh'] == 'on',
			'price' => $formData['price']
		);
		$this->db->insert($this->TABLE_NAME, $data);
		return $this->selectByHash($data['hash']);
	}

	public function approveRegistration($id)
	{
		$data = array(
			'admin_approved' => TRUE
		);
		$this->db->where('id', $id);
		$this->db->limit(1);
		$this->db->update($this->TABLE_NAME, $data);
	}

	public function selectByHash($hash = '')
	{
		$this->db->where('hash', $hash);
		$this->db->limit(1);
		$query = $this->db->get($this->TABLE_NAME);
		return $query->row();
	}

	public function userApprove($hash = '')
	{
		$data = array(
			'user_approved' => TRUE
		);
		$this->db->where('hash', $hash);
		$this->db->limit(1);
		$this->db->update($this->TABLE_NAME, $data);
	}

	private function _countHash($data)
	{
		return md5($data['name'].$data['surname'].$data['email'].$data['price']);
	}

}
/*
CREATE TABLE `registration` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`hash` VARCHAR(32) NOT NULL UNIQUE,
	`user_approved` BOOLEAN NOT NULL DEFAULT FALSE,
	`admin_approved` BOOLEAN NOT NULL DEFAULT FALSE,
	`name` VARCHAR(64) NOT NULL,
	`surname` VARCHAR(64) NOT NULL,
	`email` VARCHAR(64) NOT NULL UNIQUE,
	`arrival` INT NOT NULL,
	`nights` INT NOT NULL,
	`meals` BOOLEAN NOT NULL,
	`lects` TEXT,
	`organization_help` TEXT,
	`sleigh` BOOLEAN NOT NULL,
	`price` DECIMAL(5,2) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (arrival) REFERENCES `arrival_date`(id)
);
*/