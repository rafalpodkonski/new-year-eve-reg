<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SongModel extends CI_Model {

	private $TABLE_NAME = 'songs';

	public function selectAll($page = 0, $limit = 25)
	{
		$this->db->limit($limit, $page * $limit);
		$this->db->order_by('id');
		$query = $this->db->get($this->TABLE_NAME);
		$result = $query->result();
		return array(
			'results' => $result,
			'page' => $page,
			'limit' => $limit,
			'items' => $this->db->count_all($this->TABLE_NAME)
		);
	}

	public function count()
	{
		return $this->db->count_all_results($this->TABLE_NAME);
	}

	public function insertSongs($registrationId, $songsName)
	{
		$data = array();
		for ($i = 0; $i < count($songsName); $i++) {
			$data[$i] = array(
				'registration_id' => $registrationId,
				'song_name' => html_escape($songsName[$i])
			);
		}
		$this->db->insert_batch($this->TABLE_NAME, $data);
	}

	public function insertSong($registrationId, $songName)
	{
		$data = array(
			'registration_id' => $registrationId,
			'song_name' => html_escape($songName)
		);
		$this->db->insert($this->TABLE_NAME, $data);
	}

	public function selectByRegistrationHash($hash = '')
	{
		$this->db->select("$this->TABLE_NAME.*");
		$this->db->join('registration', "$this->TABLE_NAME.registration_id = registration.id");
		$this->db->where('registration.hash', $hash);
		$query = $this->db->get($this->TABLE_NAME);
		return $query->result();
	}

}
/*
CREATE TABLE `songs` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`registration_id` INT NOT NULL,
	`song_name` TEXT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (registration_id) REFERENCES registration(id)
);
*/