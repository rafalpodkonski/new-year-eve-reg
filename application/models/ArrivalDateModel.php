<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ArrivalDateModel extends CI_Model {

	private $TABLE_NAME = 'arrival_date';

	private $ID_COLUMN = 'id';
	private $VALUE_COLUMN = 'value';

	public function selectAll()
	{
		$query = $this->db->order_by($this->VALUE_COLUMN, 'ASC')->get($this->TABLE_NAME);
		return $query->result();
	}

}
/*
CREATE TABLE `arrival_date` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`value` VARCHAR(64) NOT NULL UNIQUE
);
INSERT INTO `arrival_date` (`value`) VALUES ('30.12.2017');
INSERT INTO `arrival_date` (`value`) VALUES ('31.12.2017');
*/