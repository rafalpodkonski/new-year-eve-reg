<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	private $TABLE_NAME = 'user';

	public function login($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', sha1($password));
		$this->db->limit(1);
		$query = $this->db->get($this->TABLE_NAME);
		if ($query->num_rows() == 1) {
			return $query->row_array();
		}
	}

}
/*
CREATE TABLE `user` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`username` VARCHAR(64) NOT NULL UNIQUE,
	`password` VARCHAR(40) NOT NULL,
	`name` VARCHAR(64) NOT NULL,
	`surname` VARCHAR(64) NOT NULL
);
INSERT INTO `user` (`username`, `password`, `name`, `surname`) VALUES ('admin', '8a72f2345db83e128d76754eb01a10be31f0439f', 'SU', 'SU');
CREATE TABLE IF NOT EXISTS `ci_sessions` (
        `id` varchar(128) NOT NULL,
        `ip_address` varchar(45) NOT NULL,
        `timestamp` int(10) unsigned DEFAULT 0 NOT NULL,
        `data` blob NOT NULL,
        KEY `ci_sessions_timestamp` (`timestamp`)
);
ALTER TABLE ci_sessions ADD PRIMARY KEY (id, ip_address);
*/
