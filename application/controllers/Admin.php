<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->helper('form');
		$this->load->view('templates/header');
		$this->load->view('admin/login');
		$this->load->view('templates/footer');
	}

	public function login() {
		$this->load->library('form_validation');
		$this->_loadValidationRules();
		$postedItems = $this->input->post();
		if ($this->form_validation->run()) {
			$this->load->model('UserModel');
			$username = $postedItems['username'];
			$password = $postedItems['password'];
			$user = $this->UserModel->login($username, $password);
			if (isset($user)) {
				$this->session->set_userdata($user);
				redirect("Admin/dashboard");
			} else {
				redirect("Admin");
			}
		} else {
			redirect("Admin");
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect("Admin");
	}

	public function dashboard() {
		$this->_checkIsUserLogged();
		$this->load->model('RegistrationModel');
		$this->load->model('SongModel');
		$data = array(
			'userdata' => $this->session->userdata(),
			'registrations_all' => $this->RegistrationModel->count(),
			'registrations_user' => $this->RegistrationModel->count(array('user_approved' => TRUE)),
			'registrations_admin' => $this->RegistrationModel->count(array('admin_approved' => TRUE)),
			'songs_all' => $this->SongModel->count()
		);
		$this->load->view('templates/header');
		$this->load->view('templates/admin_menu');
		$this->load->view('admin/dashboard', $data);
		$this->load->view('templates/footer');
	}

	public function reservations($page = 0) {
		$this->_checkIsUserLogged();
		$this->load->model('RegistrationModel');
		$data = array(
			'userdata' => $this->session->userdata(),
			'reservations' => $this->RegistrationModel->selectAll($page)
		);
		$this->load->view('templates/header');
		$this->load->view('templates/admin_menu');
		$this->load->view('admin/reservations', $data);
		$this->load->view('templates/footer');
	}

	public function approve($id) {
		$this->_checkIsUserLogged();
		if (isset($id)) {
			$this->load->model('RegistrationModel');
			$this->RegistrationModel->approveRegistration($id);
		}
		redirect('Admin/reservations');
	}

	public function songs($page = 0) {
		$this->_checkIsUserLogged();
		$this->load->model('SongModel');
		$data = array(
			'userdata' => $this->session->userdata(),
			'songs' => $this->SongModel->selectAll($page, 5)
		);
		$this->load->view('templates/header');
		$this->load->view('templates/admin_menu');
		$this->load->view('admin/songs', $data);
		$this->load->view('templates/footer');
	}

	private function _loadValidationRules() {
		$config = array(
			array(
				'field' => 'username',
				'label' => 'lang:username_field',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'password',
				'label' => 'lang:password_field',
				'rules' => 'trim|required'
			)
		);
		$this->form_validation->set_rules($config);
	}

	private function _checkIsUserLogged() {
		$result = $this->session->has_userdata('username');
		if (!$result) {
			redirect('Admin');
		}
	}

}
