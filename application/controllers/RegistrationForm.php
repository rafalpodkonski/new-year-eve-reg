<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistrationForm extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('ArrivalDateModel');

		$this->load->helper('form');
	}

	public function index()
	{
		$data = array(
			'arrival_dates' => $this->ArrivalDateModel->selectAll()
		);

		$this->load->view('templates/header');
		$this->load->view('registration_form', $data);
		$this->load->view('templates/footer');
	}

	public function save() {
		$this->load->library('form_validation');
		$this->_loadValidationRules();

		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$this->load->model('RegistrationModel');
			$this->load->model('SongModel');
			$formData = $this->input->post();
			$formData['price'] = $this->_countPrice($formData);
			$result = $this->RegistrationModel->registerUser($formData);
			if (isset($formData['songs'])) {
				$this->SongModel->insertSongs($result->id, $formData['songs']);
			}
			// $this->_sendMail($result);
			redirect("Registration/registrationDetails/$result->hash");
		}
	}

	private function _loadValidationRules() {
		$config = array(
			array(
				'field' => 'name',
				'label' => 'lang:name_field',
				'rules' => 'trim|required|min_length[3]'
			),
			array(
				'field' => 'surname',
				'label' => 'lang:surname_field',
				'rules' => 'trim|required|min_length[3]'
			),
			array(
				'field' => 'email',
				'label' => 'lang:email_field',
				'rules' => 'trim|required|valid_email'
			),
			array(
				'field' => 'arrival',
				'label' => 'lang:arrival_field',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'nights_no',
				'label' => 'lang:nights_no_field',
				'rules' => 'trim|required|numeric|greater_than_equal_to[1]|less_than_equal_to[3]'
			)
		);

		$this->form_validation->set_rules($config);
	}

	private function _countPrice($data) {
		$this->load->model('PriceModel');

		$newYearEvePartyPrice = $this->PriceModel->selectPrice('party')->value;
		$accomodationPrice = $this->PriceModel->selectPrice('accomodation')->value;
		$mealPrice = $this->PriceModel->selectPrice('meal')->value;
		$sleighPrice = $this->PriceModel->selectPrice('sleigh')->value;
		$organizationPrice = $this->PriceModel->selectPrice('organization')->value;

		$result = 0.0;
		$result += ($accomodationPrice * $data['nights_no']);
		$result += $newYearEvePartyPrice;
		$result += $organizationPrice;

		if ($data['with_meal'] == 'on') {
			$result += ($mealPrice * $data['nights_no']);
		}
		if ($data['sleigh'] == 'on') {
			$result += $sleighPrice;
		}

		return $result;
	}

	private function _sendMail($data) {
		$this->load->library('email');

		$this->email->from('sylwester.mazury@gmail.com');
		$this->email->to($data->email);
		$this->email->subject('Rezerwacja Sylwester');
		$this->email->message('Test message');
		$this->email->send();
		$this->email->print_debugger();
	}

}
