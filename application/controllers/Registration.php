<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('RegistrationModel');
	}

	public function registrationDetails($hash = '') {
		$this->load->helper('multiplier');
		$this->load->model('PriceModel');
		$this->load->model('SongModel');
		$data = array(
			'registration' => $this->RegistrationModel->selectByHash($hash),
			'priceList' => $this->PriceModel->selectAll(),
			'songs' => $this->SongModel->selectByRegistrationHash($hash)
		);

		$this->load->view('templates/header');
		$this->load->view('registration_details', $data);
		$this->load->view('templates/footer');
	}

	public function approveRegistration($hash = '') {
		$this->RegistrationModel->userApprove($hash);
		redirect("Registration/registrationDetails/$hash");
	}

}