<?php

$lang['brand'] = "Sylwester Krzyże";

$lang['registration_page'] = "Rejestracja";
$lang['registration_details_page'] = "Szczegóły Rejestracji";

$lang['personal_data_sec'] = "Twoje Dane";
$lang['stay_sec'] = "Pobyt";
$lang['meals_sec'] = "Posiłki";
$lang['organization_sec'] = "Organizacja";
$lang['entertainment_sec'] = "Rozrywki";

$lang['name_field'] = "Imię";
$lang['surname_field'] = "Nazwisko";
$lang['email_field'] = "Adres E-Mail";
$lang['arrival_field'] = "Data przyjazdu";
$lang['nights_no_field'] = "Liczba nocy";
$lang['with_meal_field'] = "Z posiłkiem";
$lang['travel_lect_field'] = "Czy chcesz poprowadzić prelekcję podróżniczą";
$lang['travel_lect_subj_field'] = "Wpisz swoje tematy";
$lang['is_org_help_field'] = "Czy chcesz pomóc w organizacji imprezy";
$lang['org_help_field'] = "W jaki sposób możesz pomóc";
$lang['sleigh_field'] = "Kulig";
$lang['username_field'] = "Nazwa Użytkownika";
$lang['password_field'] = "Hasło";

$lang['register_btn'] = "Zarejestruj";
$lang['clear_btn'] = "Wyczyść";
$lang['login_btn'] = "Zaloguj";

$lang['meal_info'] = "Do Twojej rezerwacji zostanie doliczone 35 PLN / dzień. Wyżywienie zawiera:<ul><li>30.12.2017 - Obiadokolacja</li><li>31.12.2017 - Śniadanie + Obiadokolacja</li><li>01.01.2018 - Śniadanie + Obiadokolacja</li><li>02.01.2018 - Śniadanie</li></ul>";
$lang['sleigh_info'] = "Do Twojej rezerwacji zostanie doliczone 15 PLN.";
$lang['addSong_field'] = "Dodaj piosenkę";
$lang['addSong_tip'] = "(Wpisz wykonawca - tytuł i zatwierdź klawiszem <kbd>Enter</kbd> lub zielonym plusem)";
$lang['selected_songs_panel'] = "Wybrane piosenki";

$lang['reservation_number'] = "Numer rezerwacji";
$lang['counted_price'] = "Wyliczona cena";
$lang['price_list_name'] = "Nazwa pozycji";
$lang['price_list_price'] = "Cena jednostkowa";
$lang['price_list_multiplier'] = "Mnożnik";
$lang['price_list_value'] = "Wartość";

$lang['party'] = "Impreza sylwestrowa";
$lang['accomodation'] = "Zakwaterowanie";
$lang['meal'] = "Posiłki";
$lang['sleigh'] = "Kulig";
$lang['organization'] = "Składka organizacyjna";

$lang['sum'] = "Razem";
$lang['approve'] = "Potwierdź";
$lang['songs_sent'] = "Piosenki, które nam wysłałeś/aś";
$lang['song_title'] = "Tytuły piosenek";

$lang['dashboard'] = "Dashboard";
$lang['reservations'] = "Rezerwacje";
$lang['songs'] = "Piosenki";
$lang['logout'] = "Wyloguj";

$lang['user_approved'] = "Potwierdzenie";
$lang['admin_approved'] = "Potwierdzenie admina";

$lang['yes'] = "Tak";
$lang['no'] = "Nie";

$lang['dashboard_registrations_all'] = "Wszystkich rezerwacji";
$lang['dashboard_registrations_user'] = "Rezerwacji zatwierdzonych przez użytkownika";
$lang['dashboard_registrations_admin'] = "Rezerwacji zatwierdzonych przez administratora";
$lang['dashboard_songs_all'] = "Przesłanych piosenek";
