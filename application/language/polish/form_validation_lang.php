<?php

$lang['required'] = "Pole {field} jest wymagane.";
$lang['min_length'] = "Pole {field} musi zawierać co najmniej {param} znaki";
$lang['valid_email'] = "Pole {field} musi zawierać poprawny adres poczty E-Mail";
$lang['numeric'] = "Pole {field} musi zawierać tylko liczby";
$lang['greater_than_equal_to'] = "Pole {field} musi zawierać wartość większą od {param}";
$lang['less_than_equal_to'] = "Pole {field} musi zawierać wartość mniejszą od {param}";