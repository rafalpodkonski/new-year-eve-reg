<?php

function countMultiplier($priceData, $registrationData)
{
	if ($priceData->name == 'meal' && !$registrationData->meals) {
		return 0;
	}
	if ($priceData->name == 'sleigh' && !$registrationData->sleigh) {
		return 0;
	}
	if ($priceData->daily_multiplier) {
		return $registrationData->nights;
	}
	else {
		return 1;
	}
}