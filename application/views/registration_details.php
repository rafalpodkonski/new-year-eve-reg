<div class="row">
	<div class="col-xs-6">
		<h2>
			<?= lang('registration_details_page') ?>
		</h2>
	</div>
	<div class="col-xs-offset-3 col-xs-3">
		<?php
			if(!$registration->user_approved && !$registration->admin_approved):
		?>
		<div class="alert alert-danger">
			Niepotwierdzona
		</div>
		<?php
			elseif ($registration->user_approved && !$registration->admin_approved):
		?>
		<div class="alert alert-warning">
			Potwierdzona przez Ciebie
		</div>
		<?php
			else:
		?>
		<div class="alert alert-success">
			Potwierdzona
		</div>
		<?php
			endif;
		?>
	</div>
</div>
<?php
	if (!$registration->admin_approved):
?>
<div class="row">
	<div class="col-xs-6 col-xs-offset-3">
		<div class="alert alert-info">
			Aby Twoja rezerwacja została potwierdzona dokonaj wpłaty <b><?= $registration->price ?> PLN</b> na konto:<br>
			<b>49 2490 0005 0000 4101 0013 4053</b><br>
			W tytule przelewu wpisz:<br>
			<b>Sylwester2017 (<?= $registration->id ?>)</b><br>
			Po zaksięgowaniu wpłaty Twoja rezerwacja zostanie potwierdzona po czym zapraszamy do wspólnej zabawy.
		</div>
	</div>
</div>
<?php
	endif;
?>
<div class="row">
	<div class="col-md-2">
		<b><?= lang('reservation_number') ?></b>
	</div>
	<div class="col-md-4">
		<i><?= $registration->id ?></i>
	</div>
	<div class="col-md-2">
		<b><?= lang('counted_price') ?></b>
	</div>
	<div class="col-md-4">
		<i><?= $registration->price ?> PLN</i>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<b><?= lang('name_field') ?></b>
	</div>
	<div class="col-md-4">
		<i><?= $registration->name ?></i>
	</div>
	<div class="col-md-2">
		<b><?= lang('surname_field') ?></b>
	</div>
	<div class="col-md-4">
		<i><?= $registration->surname ?></i>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<b><?= lang('email_field') ?></b>
	</div>
	<div class="col-md-4">
		<i><?= $registration->email ?></i>
	</div>
	<div class="col-md-2">
		<b><?= lang('nights_no_field') ?></b>
	</div>
	<div class="col-md-4">
		<i><?= $registration->nights ?></i>
	</div>
</div>
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th><?= lang('price_list_name') ?></th>
			<th><?= lang('price_list_price') ?></th>
			<th><?= lang('price_list_multiplier') ?></th>
			<th><?= lang('price_list_value') ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($priceList as $priceItem) {
				$name = lang($priceItem->name);
				$itemPrice = $priceItem->value;
				$multiplier = countMultiplier($priceItem, $registration);
				$value = $itemPrice * $multiplier;
				echo "<tr><td>$name</td><td>$itemPrice PLN</td><td>$multiplier</td><td>$value PLN</td></tr>";
			}
		?>
		<tr>
			<td class="text-right" colspan="3"><?= lang('sum') ?></td>
			<td><?= $registration->price ?> PLN</td>
		</tr>
	</tbody>
</table>
<fieldset>
	<legend><?= lang('songs_sent') ?></legend>
	<table class="table table-striped table-hover">
		<tbody>
			<?php
				foreach ($songs as $song):
			?>
			<tr>
				<td><?= $song->song_name ?></td>
			</tr>
			<?php
				endforeach;
			?>
		</tbody>
	</table>
</fieldset>
<?php
	if (!$registration->user_approved):
?>
<div class="row">
	<div class="col-xs-offset-9 col-xs-3">
		<a class="btn btn-success" href="<?= base_url() ?>index.php/Registration/approveRegistration/<?= $registration->hash ?>"><?= lang('approve') ?></a>
	</div>
</div>
<?php
	endif;
?>