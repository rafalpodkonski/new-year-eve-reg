<nav class="navbar navbar-inverse ">
	<?= anchor('Admin/dashboard', lang('brand'), array('class' => 'navbar-brand')) ?>
	<ul class="nav navbar-nav">
		<li><?= anchor('Admin/dashboard', lang('dashboard')) ?></li>
		<li><?= anchor('Admin/reservations', lang('reservations')) ?></li>
		<li><?= anchor('Admin/songs', lang('songs')) ?></li>
		<li><?= anchor('Admin/logout', lang('logout')) ?></li>
	</ul>
</nav>
