<h2><?= lang('registration_page') ?></h2>
<?= form_open('RegistrationForm/save') ?>
<!-- Reg form -->
	<fieldset>
		<legend><?= lang('personal_data_sec') ?></legend>
		<!-- Dane -->
		<div class="form-group">
			<label for="name"><?= lang('name_field') ?></label>
			<input type="text" name="name" id="name" class="form-control" placeholder="<?= lang('name_field') ?>" required="required" value="<?= set_value('name') ?>" />
			<?= form_error('name') ?>
		</div>
		<div class="form-group">
			<label for="surname"><?= lang('surname_field') ?></label>
			<input type="text" name="surname" id="surname" class="form-control" placeholder="<?= lang('surname_field') ?>" required="required" value="<?= set_value('surname') ?>" />
			<?= form_error('surname') ?>
		</div>
		<div class="form-group">
			<label for="email"><?= lang('email_field') ?></label>
			<input type="email" name="email" id="email" class="form-control" placeholder="<?= lang('email_field') ?>" required="required" value="<?= set_value('email') ?>" />
			<?= form_error('email') ?>
		</div>
	</fieldset>
	<fieldset>
		<legend><?= lang('stay_sec') ?></legend>
		<!-- Dane dot pobytu -->
		<div class="form-group">
			<label for="arrival"><?= lang('arrival_field') ?></label>
			<select name="arrival" id="arrival" class="form-control" required="required">
				<option <?= set_select('arrival', '', TRUE) ?>></option>
				<?php
					foreach ($arrival_dates as $arrival_date) {
						$selected = set_select('arrival', $arrival_date->id);
						echo "<option value='$arrival_date->id' $selected>$arrival_date->value</option>";
					}
				?>
			</select>
			<?= form_error('arrival') ?>
		</div>
		<div class="form-group">
			<label for="nights_no"><?= lang('nights_no_field') ?></label>
			<input type="number" name="nights_no" id="nights_no" class="form-control" placeholder="<?= lang('nights_no_field') ?>" min="1" max="3" value="<?= set_value('nights_no') ?>" />
			<?= form_error('nights_no') ?>
		</div>
	</fieldset>
	<fieldset>
		<legend><?= lang('meals_sec') ?></legend>
		<!-- Dane dot posiłków -->
		<div class="checkbox">
			<label><input type="checkbox" name="with_meal" onchange="toggle('meal_section')" value="on" <?= set_checkbox('with_meal', 'on') ?> /> <?= lang('with_meal_field') ?></label>
		</div>
		<div id="meal_section" class="well" style="display: none;">
			<?= lang('meal_info') ?>
		</div>
	</fieldset>
	<fieldset>
		<legend><?= lang('organization_sec') ?></legend>
		<!-- Dane dot organizacji -->
		<div class="checkbox">
			<label><input type="checkbox" name="travel_lect" onchange="toggle('lect_section');" value="on" <?= set_checkbox('travel_lect', 'on') ?> /> <?= lang('travel_lect_field') ?></label>
		</div>
		<div id="lect_section" class="form-group" style="display: none;">
			<label for="travel_lect_subj"><?= lang('travel_lect_subj_field') ?></label>
			<textarea name="travel_lect_subj" id="travel_lect_subj" class="form-control" placeholder="<?= lang('travel_lect_subj_field') ?>"><?= set_value('travel_lect_subj') ?></textarea>
		</div>
		<div class="checkbox">
			<label><input type="checkbox" name="is_org_help" onchange="toggle('help_section');" value="on" <?= set_checkbox('is_org_help', 'on') ?> /> <?= lang('is_org_help_field') ?></label>
		</div>
		<div id="help_section" class="form-group" style="display: none;">
			<label for="org_help"><?= lang('org_help_field') ?></label>
			<textarea name="org_help" id="org_help" class="form-control" placeholder="<?= lang('org_help_field') ?>"><?= set_value('org_help') ?></textarea>
		</div>
	</fieldset>
	<fieldset>
		<legend><?= lang('entertainment_sec') ?></legend>
		<!-- dane dot. rozrywek -->
		<div class="checkbox">
			<label><input type="checkbox" name="sleigh" onchange="toggle('sleigh');" value="on" <?= set_checkbox('sleigh', 'on') ?>><?= lang('sleigh_field') ?></label>
			<div id="sleigh" class="well" style="display: none;">
				<?= lang('sleigh_info') ?>
			</div>
		</div>
		<div class="form-group">
			<label for="addSong"><?= lang('addSong_field') ?></label>
			<small><?= lang('addSong_tip') ?></small>
			<div class="input-group">
				<input type="text" id="addSong" class="form-control" placeholder="<?= lang('addSong_field') ?>" />
				<div class="input-group-btn">
					<button type="button" class="btn btn-success" id="btnAddSong" onclick="addSongToList();"><span class="glyphicon glyphicon-plus"></span></button>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><?= lang('selected_songs_panel') ?></h3>
			</div>
			<div id="songsList" class="panel-body"></div>
		</div>
	</fieldset>

	<div class="row">
		<div class="col-md-offset-10 col-md-3">
			<button type="submit" class="btn btn-primary"><?= lang('register_btn') ?></button>
			<button type="reset" class="btn btn-danger"><?= lang('clear_btn') ?></button>
		</div>
	</div>
<?= form_close() ?>
<script type="text/javascript">
	var songs = [];
	songs = songs.concat(<?= json_encode(set_value('songs[]')) ?>);
</script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/registration-form.js"></script>
