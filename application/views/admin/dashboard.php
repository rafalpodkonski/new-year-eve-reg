<fieldset>
	<legend><?= lang('reservations') ?></legend>
	<div class="form-group">
		<label class="col-xs-4 control-label"><?= lang('dashboard_registrations_all') ?></label>
		<div class="col-xs-8">
			<p class="form-control-static"><?= $registrations_all ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-xs-4 control-label"><?= lang('dashboard_registrations_user') ?></label>
		<div class="col-xs-8">
			<p class="form-control-static"><?= $registrations_user ?> / <?= round(($registrations_user / $registrations_all) * 100, 2) ?>%</p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-xs-4 control-label"><?= lang('dashboard_registrations_admin') ?></label>
		<div class="col-xs-8">
			<p class="form-control-static"><?= $registrations_admin ?> / <?= round(($registrations_admin / $registrations_all) * 100, 2) ?>%</p>
		</div>
	</div>
</fieldset>
<fieldset>
	<legend><?= lang('songs') ?></legend>
	<div class="form-group">
		<label class="col-xs-4 control-label"><?= lang('dashboard_songs_all') ?></label>
		<div class="col-xs-8">
			<p class="form-control-static"><?= $songs_all ?></p>
		</div>
	</div>
</fieldset>
