<div class="row">
	<div class="col-xs-12">
		<h4><?= lang('reservations') ?></h4>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th><?= lang('reservation_number') ?></th>
					<th><?= lang('name_field') ?></th>
					<th><?= lang('surname_field') ?></th>
					<th><?= lang('email_field') ?></th>
					<th><?= lang('nights_no_field') ?></th>
					<th><?= lang('with_meal_field') ?></th>
					<th><?= lang('sleigh_field') ?></th>
					<th><?= lang('price_list_value') ?></th>
					<th><?= lang('user_approved') ?></th>
					<th><?= lang('admin_approved') ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($reservations['results'] as $reservation): ?>
					<tr>
						<td><?= $reservation->id ?></td>
						<td><?= $reservation->name ?></td>
						<td><?= $reservation->surname ?></td>
						<td><?= $reservation->email ?></td>
						<td><?= $reservation->nights ?></td>
						<td><?= ($reservation->meals) ? lang('yes') : lang('no') ?></td>
						<td><?= ($reservation->sleigh) ? lang('yes') : lang('no') ?></td>
						<td><?= $reservation->price ?></td>
						<td><?= ($reservation->user_approved) ? lang('yes') : lang('no') ?></td>
						<td><?= ($reservation->admin_approved) ? lang('yes') : lang('no') ?></td>
						<td>
							<?php
								if (! $reservation->admin_approved) {
									echo anchor("Admin/approve/$reservation->id", lang('approve'), array(
										'class' => 'btn btn-success'
									));
								}
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="11" class="text-center">
						<ul class="pagination">
							<?php
								$pages = $reservations['items'] / $reservations['limit'];
								for ($i = 0; $i < round($pages); $i++) {
									echo "<li>".anchor("Admin/reservations/$i", $i+1)."</li>";
								}
							?>
						</ul>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
