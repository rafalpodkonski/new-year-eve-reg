<div class="row">
	<div class="col-xs-12">
		<h4><?= lang('songs') ?></h4>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th><?= lang('song_title') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($songs['results'] as $song): ?>
					<tr>
						<td><?= $song->song_name ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="11" class="text-center">
						<ul class="pagination">
							<?php
								$pages = $songs['items'] / $songs['limit'];
								for ($i = 0; $i < round($pages); $i++) {
									echo "<li>".anchor("Admin/songs/$i", $i+1)."</li>";
								}
							?>
						</ul>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
