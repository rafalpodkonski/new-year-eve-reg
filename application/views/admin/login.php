<div class="col-xs-6 col-xs-offset-3">
	<form method="post" action="<?= base_url() ?>index.php/Admin/login">
		<div class="form-group">
			<label for="username"><?= lang('username_field') ?></label>
			<input type="text" name="username" id="username" class="form-control" />
			<?= form_error('username') ?>
		</div>
		<div class="form-group">
			<label for="password"><?= lang('password_field') ?></label>
			<input type="password" name="password" id="password" class="form-control" />
			<?= form_error('password') ?>
		</div>
		<div class="row col-xs-12">
			<button class="btn btn-primary form-control"><?= lang('login_btn') ?></button>
		</div>
	</form>
</div>
