function load() {
	var config = [{
		checkboxSelector: 'input[name=with_meal]',
		sectionSelector: 'meal_section'
	}, {
		checkboxSelector: 'input[name=travel_lect]',
		sectionSelector: 'lect_section'
	}, {
		checkboxSelector: 'input[name=is_org_help]',
		sectionSelector: 'help_section'
	}, {
		checkboxSelector: 'input[name=sleigh]',
		sectionSelector: 'sleigh'
	}];
	config.forEach(function(element) {
		if ($(element.checkboxSelector).is(':checked')) {
			toggle(element.sectionSelector);
		}
	});
}
load();

function toggle(id) {
	$element = $('#' + id);
	var result = $element.toggle();
	$element.find('.form-control').prop('required', result.is(':visible'));
}

function addSongToList() {
	var song = $('#addSong').val();

	songs.push(song);
	refreshSongsList();

	$('#addSong').val('');
}

function refreshSongsList() {
	$songListElement = $('#songsList');
	$songListElement.empty();

	songs.forEach(function(song) {
		if (song.length > 0) {
			$spanElement = $('<span></span>');
			$spanElement.addClass('label label-info song');
			$spanElement.text(song).html();
			$songListElement.append($spanElement);
			$songListElement.append('<input type="hidden" name="songs[]" value="' + song + '" />')
		}
	});
}
refreshSongsList();

$('#addSong').keydown(function(event) {
	if (event.keyCode == 13) {
		$('#btnAddSong').click();
		event.preventDefault();
	}
});